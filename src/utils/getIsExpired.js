const getIsExpired = ({ createdAt, timeToLive }) => {
    if (!createdAt || (timeToLive !== 0 && !timeToLive)) {
        throw new Error(
            'Please provider proper parameters to validateExpiration'
        );
    }

    if (timeToLive === 0) return false;

    const limit = createdAt + timeToLive;
    const now = new Date().getTime();
    const isExpired = now > limit;

    return isExpired;
};

module.exports = getIsExpired;
