const Slot = require('./Slot');
const {
    CACHE_ADD_MESSAGES,
    CACHE_GET_MESSAGES,
    CACHE_DELETE_MESSAGES,
    EVICTION_POLICY,
} = require('../../constants');

class Cache {
    constructor({ slots, timeToLive, evictionPolicy }) {
        this.slots = slots;
        this.timeToLive = timeToLive;
        this.evictionPolicy = evictionPolicy;
        this.data = [];
    }

    createNewSlot({ key, data, timeToLive }) {
        return new Slot({
            key,
            data,
            timeToLive: Number(timeToLive ?? this.timeToLive),
        });
    }

    findSlot(key) {
        return this.data.find((item) => item.key === key);
    }

    removeSlot(key) {
        const index = this.data.findIndex((item) => item.key === key);
        this.data.splice(index, 1);
    }

    evictionPolicyOldestFirst({ key, data, timeToLive }) {
        for (let i = 0; i < this.data.length; i++) {
            const slot = this.data[i];

            if (slot.isExpired()) {
                const newSlot = this.createNewSlot({
                    key,
                    data,
                    timeToLive: timeToLive ?? this.timeToLive,
                });
                this.data.splice(i, 1, newSlot);

                return {
                    success: true,
                    message: CACHE_ADD_MESSAGES.SUCCESS_EVICTION,
                };
            }
        }

        return {
            success: false,
            message: CACHE_ADD_MESSAGES.LIMIT_REACHED,
        };
    }

    evictionPolicyNewestFirst({ key, data, timeToLive }) {
        for (let i = this.data.length - 1; i >= 0; i--) {
            const slot = this.data[i];
            if (slot.isExpired()) {
                const newSlot = this.createNewSlot({
                    key,
                    data,
                    timeToLive: timeToLive ?? this.timeToLive,
                });
                this.data.splice(i, 1, newSlot);

                return {
                    success: true,
                    message: CACHE_ADD_MESSAGES.SUCCESS_EVICTION,
                };
            }
        }

        return {
            success: false,
            message: CACHE_ADD_MESSAGES.LIMIT_REACHED,
        };
    }

    add({ key, data, timeToLive, update = false }) {
        // Evaluate posibility to force replace using parameter in body
        const slot = this.findSlot(key);

        if (slot && !update) {
            return {
                success: false,
                message: CACHE_ADD_MESSAGES.ALREADY_EXIST,
            };
        }

        if (this.data.length === this.slots) {
            if (this.evictionPolicy === EVICTION_POLICY.REJECT) {
                return {
                    success: false,
                    message: CACHE_ADD_MESSAGES.LIMIT_REACHED,
                };
            }

            if (this.evictionPolicy === EVICTION_POLICY.NEWEST_FIRST) {
                return this.evictionPolicyNewestFirst({
                    key,
                    data,
                    timeToLive,
                });
            }

            if (this.evictionPolicy === EVICTION_POLICY.OLDEST_FIRST) {
                return this.evictionPolicyOldestFirst({
                    key,
                    data,
                    timeToLive,
                });
            }
        }

        this.data.push(
            this.createNewSlot({
                key,
                data,
                timeToLive: timeToLive ?? this.timeToLive,
            })
        );

        return {
            success: true,
            message: CACHE_ADD_MESSAGES.SUCCESS,
        };
    }

    get(key) {
        const slot = this.findSlot(key);

        if (!slot) {
            return {
                success: false,
                message: CACHE_GET_MESSAGES.NOT_FOUND,
            };
        }

        const isExpired = slot?.isExpired();

        return {
            success: !isExpired,
            data: slot.data,
            message: isExpired
                ? CACHE_GET_MESSAGES.EXPIRED
                : CACHE_GET_MESSAGES.FOUND,
        };
    }

    // eslint-disable-next-line consistent-return
    delete(key) {
        const { message } = this.get(key);
        if (message === CACHE_GET_MESSAGES.FOUND) {
            this.removeSlot(key);
            return {
                success: true,
                message: CACHE_DELETE_MESSAGES.REMOVED,
            };
        }

        if (message === CACHE_GET_MESSAGES.NOT_FOUND) {
            return {
                success: false,
                message: CACHE_DELETE_MESSAGES.NOT_FOUND,
            };
        }

        if (message === CACHE_GET_MESSAGES.EXPIRED) {
            return {
                success: false,
                message: CACHE_DELETE_MESSAGES.EXPIRED,
            };
        }
    }
}

module.exports = Cache;
