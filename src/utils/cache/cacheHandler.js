const { EVICTION_POLICY } = require('../../constants');
const Cache = require('./Cache');

let cache;

const initCache = ({
    slots = 10_000,
    timeToLive = 3_600_000,
    evictionPolicy = EVICTION_POLICY.REJECT,
}) => {
    if (!cache) {
        cache = new Cache({
            slots: Number(slots),
            timeToLive: Number(timeToLive),
            evictionPolicy,
        });
    }
};

const getCache = () => {
    if (!cache) {
        initCache({
            slots: process.env.SLOTS,
            timeToLive: process.env.TIME_TO_LIVE,
            evictionPolicy: process.env.EVICTION_POLICY,
        });
    }

    return cache;
};

const destroyCache = () => {
    cache = null;
};

module.exports = {
    initCache,
    getCache,
    destroyCache,
};
