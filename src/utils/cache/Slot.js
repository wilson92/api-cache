const getIsExpired = require('../getIsExpired');

class Slot {
    constructor({ key, data, timeToLive }) {
        this.key = key;
        this.data = data;
        this.createdAt = new Date().getTime();
        this.expired = false;
        this.timeToLive = timeToLive;
    }

    isExpired() {
        if (this.expired) return true;

        const isExpired = getIsExpired({
            createdAt: this.createdAt,
            timeToLive: this.timeToLive,
        });

        this.expired = isExpired;
        return this.expired;
    }
}

module.exports = Slot;
