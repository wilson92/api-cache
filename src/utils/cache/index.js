const { initCache, getCache, destroyCache } = require('./cacheHandler');

module.exports = {
    initCache,
    getCache,
    destroyCache,
};
