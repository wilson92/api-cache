const express = require('express');
const {
    addObject,
    getObject,
    deleteObject,
    getAll,
} = require('../controllers/object.controller');

const router = express.Router();

router.get('/', getAll);

router.get('/:key', getObject);

router.post('/:key', addObject);

router.delete('/:key', deleteObject);

module.exports = router;
