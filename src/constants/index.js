const EVICTION_POLICY = {
    OLDEST_FIRST: 'OLDEST_FIRST',
    NEWEST_FIRST: 'NEWEST_FIRST',
    REJECT: 'REJECT',
};

const CACHE_ADD_MESSAGES = {
    ALREADY_EXIST: 'Key already exist',
    LIMIT_REACHED: 'Slot limit reached',
    SUCCESS: 'Slot added succesfully',
    SUCCESS_EVICTION: 'Slot placed successfully',
};

const CACHE_GET_MESSAGES = {
    FOUND: 'Found',
    NOT_FOUND: 'Not found',
    EXPIRED: 'Expired',
};

const CACHE_DELETE_MESSAGES = {
    REMOVED: 'The object was found and removed',
    NOT_FOUND: 'Not found',
    EXPIRED: 'Expired',
};

module.exports = {
    EVICTION_POLICY,
    CACHE_ADD_MESSAGES,
    CACHE_GET_MESSAGES,
    CACHE_DELETE_MESSAGES,
};
