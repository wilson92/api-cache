const { getCache } = require('../utils/cache');
const { CACHE_ADD_MESSAGES } = require('../constants');

const cache = getCache();

const addObject = (req, res) => {
    if (!req.body) {
        res.status(400);
        res.send({
            success: false,
            message: 'Request Body is empty',
        });
    }

    const response = cache.add({
        key: req.params.key,
        data: req.body,
        timeToLive: req.query.ttl,
    });

    if (response.success) {
        res.status(200);
        res.send(response);
    }

    if (
        !response.success &&
        response.message === CACHE_ADD_MESSAGES.ALREADY_EXIST
    ) {
        res.status(400);
        res.send(response);
    }

    if (
        !response.success &&
        response.message === CACHE_ADD_MESSAGES.LIMIT_REACHED
    ) {
        res.status(507);
        res.send(response);
    }
};

const getObject = (req, res) => {
    const response = cache.get(req.params.key);

    if (response.success) {
        res.status(200);
        res.send(response);
    }

    if (!response.success) {
        res.status(404);
        res.send({
            success: response.success,
            message: response.message,
        });
    }
};

const getAll = (_, res) => {
    res.status(200);
    res.send(cache.data);
};

const deleteObject = (req, res) => {
    const response = cache.delete(req.params.key);

    if (response.success) {
        res.status(200);
        res.send(response);
    }

    if (!response.success) {
        res.status(404);
        res.send(response);
    }
};

module.exports = {
    addObject,
    getObject,
    getAll,
    deleteObject,
};
