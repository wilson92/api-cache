const express = require('express');
const objectRoutes = require('./routes/object.routes');
const { initCache } = require('./utils/cache/cacheHandler');

const app = express();

// Initialize cache
initCache({
    slots: process.env.SLOTS,
    timeToLive: process.env.TIME_TO_LIVE,
    evictionPolicy: process.env.EVICTION_POLICY,
});

app.use(express.json());
app.use('/object', objectRoutes);

app.get('/health', (_, res) => {
    res.status(200);
    res.send('Server running');
});

module.exports = app;
