const request = require('supertest');
const app = require('../src/app');
const { CACHE_ADD_MESSAGES } = require('../src/constants');

describe('App', () => {
    it('GET /health', async () => {
        const response = await request(app).get('/health');

        expect(response.statusCode).toBe(200);
        expect(response.text).toBe('Server running');
    });

    describe('GET /object', () => {
        it('Should return empty data it not data was added', async () => {
            const response = await request(app).get('/object');

            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveLength(0);
        });

        it('Should return  added data', async () => {
            await request(app).post('/object/1').send({ data: 1 });

            const response = await request(app).get('/object');

            expect(response.statusCode).toBe(200);
            expect(response.body).toHaveLength(1);
        });
    });

    describe('GET /object/:key', () => {
        it('Should return 404 getting not added key', async () => {
            const response = await request(app).get('/object/FAKE_KEY');

            expect(response.statusCode).toBe(404);
        });

        it('Should return  added data by key', async () => {
            const data = { data: 1 };
            const key = 'Get_by_key';
            await request(app).post(`/object/${key}`).send(data);
            const response = await request(app).get(`/object/${key}`);

            expect(response.statusCode).toBe(200);
        });
    });

    describe('POST /object', () => {
        it('Should add object', async () => {
            const data = { data: 1 };
            const key = 'MyKey';
            const response = await request(app)
                .post(`/object/${key}`)
                .send(data);

            expect(response.statusCode).toBe(200);
            expect(response.body.success).toBe(true);
            expect(response.body.message).toBe(CACHE_ADD_MESSAGES.SUCCESS);
        });

        it('Should return 400 when adding duplicated key', async () => {
            const data = { data: 1 };
            const key = 'MyKeyDUPLICATED';
            await request(app).post(`/object/${key}`).send(data);
            const response = await request(app)
                .post(`/object/${key}`)
                .send(data);

            expect(response.statusCode).toBe(400);
        });
    });

    describe('DELETE /object', () => {
        it('Should delete object', async () => {
            const data = { data: 1 };
            const key = 'MyKeyDELETE';
            await request(app).post(`/object/${key}`).send(data);

            const response = await request(app).delete(`/object/${key}`);

            expect(response.statusCode).toBe(200);
            expect(response.body.success).toBe(true);
        });
    });
});
