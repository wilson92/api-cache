const {
    initCache,
    getCache,
    destroyCache,
} = require('../../../src/utils/cache');

const {
    EVICTION_POLICY,
    CACHE_ADD_MESSAGES,
    CACHE_GET_MESSAGES,
    CACHE_DELETE_MESSAGES,
} = require('../../../src/constants');

const forceDelay = (time) =>
    new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, time);
    });

const emptyInitParams = {
    slots: undefined,
    timeToLive: undefined,
    evictionPolicy: undefined,
};

describe('Cache', () => {
    beforeEach(() => {
        destroyCache();
    });

    describe('Initialization', () => {
        it('Should destroy cache', () => {
            initCache(emptyInitParams);

            destroyCache();

            expect(getCache().data).toEqual([]);
        });

        it('Should init with default parameters if custom parameters not provided', () => {
            initCache(emptyInitParams);

            const cache = getCache();

            expect(cache.slots).toBe(10_000);
            expect(cache.timeToLive).toBe(3_600_000);
            expect(cache.evictionPolicy).toBe(EVICTION_POLICY.REJECT);
        });

        it('Should init with provided parameters', () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
                evictionPolicy: EVICTION_POLICY.NEWEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            expect(cache.slots).toBe(params.slots);
            expect(cache.timeToLive).toBe(params.timeToLive);
            expect(cache.evictionPolicy).toBe(params.evictionPolicy);
        });
    });

    describe('Add', () => {
        it('Should add item to the cache', () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };
            const key = 'test';
            const data = {
                name: 'test test',
            };

            initCache(params);

            const cache = getCache();

            const response = cache.add({
                key,
                data,
                timeToLive: 28,
            });

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.SUCCESS);
        });

        it('Should return not success when trying to add duplicated key', () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };

            initCache(params);

            const cache = getCache();

            cache.add({
                key: 1,
                data: 1,
                timeToLive: 28,
            });

            const response = cache.add({
                key: 1,
                data: 1,
                timeToLive: 28,
            });

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.ALREADY_EXIST);
        });
    });

    describe('Evition policy reject', () => {
        it('Should return not success when cache runs out of slots', () => {
            const params = {
                slots: 1,
                timeToLive: 1_000,
                evictionPolicy: EVICTION_POLICY.REJECT,
            };

            initCache(params);

            const cache = getCache();

            cache.add({
                key: 1,
                data: 1,
                timeToLive: 28,
            });

            const response = cache.add({
                key: 2,
                data: 1,
                timeToLive: 28,
            });

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.LIMIT_REACHED);
        });
    });

    describe('Eviction policy newest first', () => {
        it('Should evict latest item', async () => {
            const params = {
                slots: 3,
                timeToLive: 10,
                evictionPolicy: EVICTION_POLICY.NEWEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 0; i < params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                });
            }

            await forceDelay(20);
            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };
            const response = cache.add(newItem);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.SUCCESS_EVICTION);

            expect(cache.data[2].key).toBe(newItem.key);
            expect(cache.data[2].data).toEqual(newItem.data);
        });

        it('Should evict second item', async () => {
            const params = {
                slots: 3,
                evictionPolicy: EVICTION_POLICY.NEWEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 1; i <= params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                    timeToLive: 200 * i,
                });
            }

            await forceDelay(400);

            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };

            const response = cache.add(newItem);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.SUCCESS_EVICTION);

            expect(cache.data[1].key).toBe(newItem.key);
            expect(cache.data[1].data).toEqual(newItem.data);
        });

        it('Should not evict if there is not expired item', () => {
            const params = {
                slots: 3,
                timeToLive: 10,
                evictionPolicy: EVICTION_POLICY.NEWEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 1; i <= params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                    timeToLive: i * 10,
                });
            }

            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };

            const response = cache.add(newItem);

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.LIMIT_REACHED);
        });
    });

    describe('Eviction policy oldest first', () => {
        it('Should evict first item', async () => {
            const params = {
                slots: 3,
                timeToLive: 10,
                evictionPolicy: EVICTION_POLICY.OLDEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 0; i < params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                });
            }

            await forceDelay(20);
            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };
            const response = cache.add(newItem);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.SUCCESS_EVICTION);

            expect(cache.data[0].key).toBe(newItem.key);
            expect(cache.data[0].data).toEqual(newItem.data);
        });

        it('Should evict second item', async () => {
            const params = {
                slots: 3,
                timeToLive: 10,
                evictionPolicy: EVICTION_POLICY.OLDEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 1; i <= params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                    timeToLive: 50 / i,
                });
            }

            await forceDelay(30);

            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };

            const response = cache.add(newItem);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.SUCCESS_EVICTION);

            expect(cache.data[1].key).toBe(newItem.key);
            expect(cache.data[1].data).toEqual(newItem.data);
        });

        it('Should not evict if there is not expired item', () => {
            const params = {
                slots: 15,
                timeToLive: 10,
                evictionPolicy: EVICTION_POLICY.OLDEST_FIRST,
            };

            initCache(params);

            const cache = getCache();

            for (let i = 1; i <= params.slots; i++) {
                cache.add({
                    key: i,
                    data: i,
                    timeToLive: i * 10,
                });
            }

            const newItem = {
                key: 'NEW',
                data: {
                    name: 'NEW',
                },
            };

            const response = cache.add(newItem);

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_ADD_MESSAGES.LIMIT_REACHED);
        });
    });

    describe('Get', () => {
        it('Should not find not added data', () => {
            initCache(emptyInitParams);

            const cache = getCache();

            const response = cache.get(1);

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_GET_MESSAGES.NOT_FOUND);
        });

        it('Should find added data', () => {
            const key = 'test';
            const data = {
                name: 'Test',
            };

            initCache(emptyInitParams);

            const cache = getCache();

            cache.add({
                key,
                data,
            });

            const response = cache.get(key);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_GET_MESSAGES.FOUND);
            expect(response.data).toEqual(data);
        });

        it('Should return success when added data has timeToLive 0', () => {
            const key = 'test';
            const data = {
                name: 'Test',
            };

            initCache(emptyInitParams);

            const cache = getCache();

            cache.add({
                key,
                data,
                timeToLive: 0,
            });

            const response = cache.get(key);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_GET_MESSAGES.FOUND);
            expect(response.data).toEqual(data);
        });

        it('Should return not success when added data has expired', async () => {
            const key = 'test';
            const data = {
                name: 'Test',
            };

            initCache(emptyInitParams);

            const cache = getCache();

            cache.add({
                key,
                data,
                timeToLive: 10,
            });

            await forceDelay(100);

            const response = cache.get(key);

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_GET_MESSAGES.EXPIRED);
            expect(response.data).toEqual(data);
        });
    });

    describe('Delete', () => {
        it('Should delete added data', () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };

            initCache(params);

            const cache = getCache();

            const key = 1;
            cache.add({
                key,
                data: 1,
                timeToLive: 28,
            });

            const response = cache.delete(key);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_DELETE_MESSAGES.REMOVED);
        });

        it('Should return not success when trying to delete not added data', () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };

            initCache(params);

            const cache = getCache();

            const response = cache.delete('Test 1 key');

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_DELETE_MESSAGES.NOT_FOUND);
        });
        it('Should return not success when trying to delete not added data', async () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };

            initCache(params);

            const cache = getCache();

            const key = 1;

            cache.add({
                key,
                data: 1,
            });

            await forceDelay(1050);

            const response = cache.delete(key);

            expect(response.success).toBe(false);
            expect(response.message).toBe(CACHE_DELETE_MESSAGES.EXPIRED);
        });

        it('Should return success when trying to delete added data with timeToLive 0', async () => {
            const params = {
                slots: 2,
                timeToLive: 1_000,
            };

            initCache(params);

            const cache = getCache();

            const key = 1;

            cache.add({
                key,
                data: 1,
                timeToLive: 0,
            });

            await forceDelay(1050);

            const response = cache.delete(key);

            expect(response.success).toBe(true);
            expect(response.message).toBe(CACHE_DELETE_MESSAGES.REMOVED);
        });
    });
});
