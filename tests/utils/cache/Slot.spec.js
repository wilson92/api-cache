const Slot = require('../../../src/utils/cache/Slot');

const forceDelay = (time) =>
    new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, time);
    });

describe('Slot', () => {
    it('Should create slot', () => {
        const slot = new Slot({
            data: 1,
            timeToLive: 1,
        });

        expect(slot.data).toBe(1);
        expect(slot.timeToLive).toBe(1);
    });

    it('Should return is not expired when timeToLive is 0', () => {
        const slot = new Slot({
            data: 'test',
            timeToLive: 0,
        });

        const isExpired = slot.isExpired();
        expect(isExpired).toBe(false);
    });

    it('Should return expired', async () => {
        const slot = new Slot({
            data: 'test',
            timeToLive: 1,
        });

        await forceDelay(100);

        const isExpired = slot.isExpired();
        expect(isExpired).toBe(true);
    });

    it('Should return is not expired', async () => {
        const slot = new Slot({
            data: 'test',
            timeToLive: 1000,
        });

        await forceDelay(100);

        const isExpired = slot.isExpired();
        expect(isExpired).toBe(false);
    });
});
