const getIsExpired = require('../../src/utils/getIsExpired');

describe('getIsExpired', () => {
    it('Should throw an error when parameters are not provided', () => {
        expect(() => {
            getIsExpired({});
        }).toThrowError();
    });

    it('Should return false when  timeToLive is 0', () => {
        const result = getIsExpired({
            createdAt: new Date().getTime(),
            timeToLive: 0,
        });

        expect(result).toBe(false);
    });

    it('Should return false when createdAt=now and timeToLive is 1 hour', () => {
        const result = getIsExpired({
            createdAt: new Date().getTime(),
            timeToLive: 3_600_000,
        });

        expect(result).toBe(false);
    });

    it('Should return false when createdAt=now and timeToLive is 1 min', () => {
        const result = getIsExpired({
            createdAt: new Date().getTime(),
            timeToLive: 60_000,
        });

        expect(result).toBe(false);
    });

    it('Should return true when createad at was 2h ago and timeToLive is 1h', () => {
        const result = getIsExpired({
            createdAt: new Date().getTime() - 7_200_000,
            timeToLive: 3_600_000,
        });

        expect(result).toBe(true);
    });

    it('Should return true when createad at was 1h ago and timeToLive is 1min', () => {
        const result = getIsExpired({
            createdAt: new Date().getTime() - 3_600_000,
            timeToLive: 1_000,
        });

        expect(result).toBe(true);
    });
});
