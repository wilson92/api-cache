# Api-cache
NodeJs implementation for an in-memory cache with a REST interface.

## Tech Stack
- Js
- Node js
- Express
- Jest
- Supertest
- Eslint
- Prettier
- Nodemon

## How to Run
Make sure you have nodejs and npm installed, this project was built using node 14.

1. Create a `.env` file, this step is optional as there are default values for each field:
    ```sh
    PORT=####
    SLOTS=####
    TIME_TO_LIVE=####
    EVICTION_POLICY=####
    ```
    - PORT (default 3000) port to start the server
    - SLOTS (default 10000) max capacity for in-memory cache
    - EVICTION_POLICY (default REJECT) can have 3 values OLDEST_FIRST, NEWEST_FIRST, REJECT
    - TIME_TO_LIVE (default 3600000) live time in miliseconds for added data
2. Run `npm i`
3. Run `npm run dev`
4. Enjoy

## Available scripts
 - `npm start` starts the server
 - `npm run dev` starts the server in dev mode
 - `npm test` runs the test suite
 - `npm run test:watch` runs the test in watch mode
 - `npm run lint` linting for code
 - `npm run lint:fix` fix lint for code

## Rest API operacion
- GET /object/{key}
    This will return the object stored at {key} if the object is not expired.
    Returns:
    ■ 200: If the object is found and not-expired
    ■ 404: If the object is not found or expired

- POST /object/{key}?ttl={ttl}
    This will insert the {object} provided in the body of the request into a slot in memory at {key}. If {ttl} is not specified it will use server’s default TTL from the config, if ttl=0 it means store indefinitely
    Returns
    ■ 200: If the server was able to store the object
    ■ 507: If the server has no storage

- DELETE /object/{key}
    This will delete the object stored at slot {key}
    Returns
    ■ 200: If the object at {key} was found and removed
    ■ 404: If the object at {key} was not found or expired





